# Specify parent image. Please select a fixed tag here.
ARG BASE_IMAGE=registry.git.rwth-aachen.de/jupyter/profiles/rwth-courses
FROM ${BASE_IMAGE}

COPY data data

COPY img img

USER root

RUN sudo apt-get update && apt-get install -y libgeos-dev gcc make

USER jovyan

# Install packages via requirements.txt
ADD requirements.txt .
RUN pip install -r requirements.txt

